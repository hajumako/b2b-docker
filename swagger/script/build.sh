#!/bin/bash
clear
echo "==================="
echo "swagger editor build script"
echo "==================="
echo "Build is starting..."
now=$(date +"%T")
echo "START time : $now"
echo "==================="

source .env

echo "npm install..."
npm update ${PROJECT_PATH}
npm install ${PROJECT_PATH}
echo "Done."
echo "==================="


echo "build swagger editor..."
npm run build ${PROJECT_PATH}
echo "Done."
echo "==================="

now=$(date +"%T")
echo "END Time : $now"

echo "The build of the swagger editor has been completed. Enjoy!"
echo "==================="
