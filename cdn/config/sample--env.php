<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 29.06.2017
 * Time: 09:51
 */

return [
  'debug_config' => [
    'debug_mode' => true,
    'debug_level' => 100, //levels described in \vendor\monolog\monolog\src\Monolog\Logger.php
    'allow_all_origins' => true,
    'log_exceptions' => true, //always log exception to log file
    'slim_handler' => false, //errors handled by slim
    'log_path' => '../../logs/app.log',
    'user_id' => 25377, //user id used in debug mode
  ],
  'tested_urls' => [ //URLs exposed for direct call, without autorization check, debug_config->user_id used as user id
  ],
  'jwt_passthrough_urls' => [ //URLs not checked by jwt middleware
  ],
  'settings.displayErrorDetails' => true,
  'settings.addContentLengthHeader' => false,
  'db' => [
    'host' => '',
    'port' => '',
    'dbname' => '',
    'user' => '',
    'pass' => '',
  ],
  'expires' => 0, //seconds
  'img_host' => '', //url (full path) to folder with orignal images (i.e. termaheat.pl/images/)
  'cdn_host' => '',
  'allowed_origins' => [
    '*',
  ],
  'images_url' => '', //url to folder with images on cdn
  'images_path' => __DIR__.'', //path to folder with images on cdn
  'root_dir' => __DIR__,
  'static_folder' => '', //directory where static (cached) files are stored
  'def_view' => '', //default camera used for SKU url (CAM0, CAM1, CAM2, CAM3, CAM4, CAM5)
  'others_folder' => '', //folder with images of uncategorized accessories
  'ax_webservices' => [
    '' => [ //webservice name
      'host' => '',
      'user' => '',
      'pass' => '',
      'functions' => [ //list of functions to swagger

      ],
      'options' => [
        'authentication' => SOAP_AUTHENTICATION_BASIC,
        'connection_timeout' => 60,
        'exceptions' => true,
      ],
    ],
  ],
  'mongodb' => [
    'host' => '',
    'user' => '',
    'pass' => '',
    'dbname' => '',
  ],
];
