#!/bin/bash
clear
echo "==================="
echo "CDN build script"
echo "==================="
echo "Build is starting..."
now=$(date +"%T")
echo "START time : $now"
echo "==================="

source .env

echo "Copying env.php"

echo "ENV"
yes | cp -rf /cdn_config/env.php ${PROJECT_PATH}/
echo "Done."
echo "==================="

echo "composer install..."
composer install -d ${PROJECT_PATH}
echo "Done."
echo "==================="

echo "npm install..."
npm install ${PROJECT_PATH}
echo "Done."
echo "==================="

now=$(date +"%T")
echo "END Time : $now"

echo "The build of the CDN has been completed. Enjoy!"
echo "==================="
