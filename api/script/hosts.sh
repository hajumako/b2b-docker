#!/bin/bash

source /api_script/.env

echo "add terma24 to hosts..."
echo "${T24_IP} terma24.local www.terma24.local" >> /etc/hosts
echo "Done."
echo "==================="

# execute apache
exec "apache2-foreground"