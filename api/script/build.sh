#!/bin/bash
clear
echo "==================="
echo "API build script"
echo "==================="
echo "Build is starting..."
now=$(date +"%T")
echo "START time : $now"
echo "==================="
if [ ! -f /databases/import/db.sql ]; then
    echo "File not found!"
fi

source .env

echo "Copying env.php"

echo "ENV"
yes | cp -rf /api_config/env.php ${PROJECT_PATH}/
echo "Done."
echo "===================" 
echo "USER: ${MYSQL_USER}"

if [ "$1" == true ]
  then
echo "STEP 1: Drop database..."
    mysql -h mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} -e "CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE}"
    mysql -h mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} -D ${MYSQL_DATABASE} -e "DROP DATABASE ${MYSQL_DATABASE}"
    echo "Done."
    echo "==================="

    echo "STEP 2: Import database..."
    mysql -h mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} -e "CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE}"
    pv ${DB_DUMP_LOCATION} | mysql -h mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE}
    echo "Import B2B schema..."
    pv '/var/www/html/db_schema/terma_api_schema.sql' | mysql -h mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE}
    echo "Done."

    if [ "$2" == 'beta' ]
       then
        mysql -h mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} -D ${MYSQL_DATABASE} -e "INSERT INTO shared_users (uid, name, pass, mail, theme, signature, signature_format, created, access, login, status, timezone, language, picture, init, data, uuid) VALUES (25377, 'ph_test', '\$S\$DXzpZulyobndH9sFtweQ9jK4LECNUtfkAEsCsp.pclT60rCEsOBe', 'ph@ph.pl', '', '', 'advanced_html', 1501668718, 1501749248, 1501749248, 1, NULL, 'pl', 0, 'ph@ph.pl', 0x613A383A7B733A31363A22636B656469746F725F64656661756C74223B733A313A2274223B733A32303A22636B656469746F725F73686F775F746F67676C65223B733A313A2274223B733A31343A22636B656469746F725F7769647468223B733A343A2231303025223B733A31333A22636B656469746F725F6C616E67223B733A323A22656E223B733A31383A22636B656469746F725F6175746F5F6C616E67223B733A313A2274223B733A31373A226D696D656D61696C5F746578746F6E6C79223B623A303B733A31383A2268746D6C6D61696C5F706C61696E74657874223B693A303B733A373A226F7665726C6179223B693A313B7D, 'f4a56e01-a89b-4ddf-88b4-c26f872c36e3');"
        mysql -h mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} -D ${MYSQL_DATABASE} -e "INSERT INTO tstore_field_data_field_ax_id (entity_type, bundle, deleted, entity_id, revision_id, language, delta, field_ax_id_value, field_ax_id_format) VALUES ('user', 'user', 0, 25377, 25377, 'und', 0, 'C7070', NULL);"
    fi
else
    echo "Drop and import of database will be omitted..."
fi
echo "==================="

echo "add terma24 to hosts..."
echo "${T24_IP} terma24.local www.terma24.local" >> /etc/hosts
echo "Done."
echo "==================="

echo "composer install..."
composer install -d ${PROJECT_PATH}
echo "Done."
echo "==================="

echo "npm install..."
npm install ${PROJECT_PATH}
echo "Done."
echo "==================="

now=$(date +"%T")
echo "END Time : $now"

echo "The build of the API has been completed. Enjoy!"
echo "==================="
