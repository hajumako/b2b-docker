<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 29.06.2017
 * Time: 09:51
 */

return [
  'debug_config' => [
    'debug_mode' => true,
    'debug_level' => 100, //levels described in \vendor\monolog\monolog\src\Monolog\Logger.php
    'allow_all_origins' => true,
    'log_exceptions' => true, //always log exception to log file
    'slim_handler' => false, //errors handled by slim
    'log_path' => '../../logs/app.log',
    'user_id' => 0, //user id used in debug mode
  ],
  'tested_urls' => [ //URLs exposed for direct call, without autorization check, debug_config->user_id used as user id
  ],
  'jwt_passthrough_urls' => [ //URLs not checked by jwt middleware
  ],
  'settings.displayErrorDetails' => true,
  'settings.addContentLengthHeader' => false,
  'db' => [
    'host' => '',
    'port' => '',  //default 3306
    'dbname' => '',
    'user' => '',
    'pass' => ''
  ],
  'img_host' => '', //url to directory with images from drupal
  'cdn_host' => '',
  'def_cam' => '', //default camera for cdn images
  'api_host' => '',
  'api_schema' => '', //http or https
  'allowed_domains' => [
    //domains
  ],
  'allowed_origins' => [
    //domains
  ],
  'allowed_tld' => '',
  'token_settings' => [
    'salt' => '',
    'name' => '',
    'valid_for' => 0,  //hours
    'reissue' => 0   //minutes
  ],
  'root_dir' => __DIR__,
  'outlet_user' => '', //C from AX assigned for outlet
  'ax_webservices' => [
    '' => [ //webservice name
      'host' => '',
      'user' => '',
      'pass' => '',
      'functions' => [ //list of functions to swagger

      ],
      'options' => [
        'authentication' => SOAP_AUTHENTICATION_BASIC,
        'connection_timeout' => 60,
        'exceptions' => true,
      ],
    ],
  ],
  'payu' => [
    'continueUrl' => '',
    'notifyUrl' => '',
    'environment' => 'sandbox',
    'pos_id' => '',
    'md5' => '',
    'client_secret' => '',
  ],
  'mongodb' => [
    'host' => '',
    'user' => '',
    'pass' => '',
    'dbname' => '',
  ],
];
